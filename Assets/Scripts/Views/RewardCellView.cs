﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using EnhancedUI.EnhancedScroller;
using EnhancedUI;

public class RewardCellView : EnhancedScrollerCellView
{

    public Image rewardImage;
    public Text rewardName;
    public Text rewardPrice;
    private string price;
    public Button buyButton;

    private RewardController rewardController;
    // Use this for initialization

    public void SetData(Reward data, RewardController controller)
    {
        LoadData(data);
        rewardController = controller;
        buyButton.onClick.AddListener(CheckPointsAndShowMessage);
    }

    public void CheckPointsAndShowMessage()
    {
        int targetPoints = int.Parse(price);
        if(targetPoints >= GlobalData.Instance.Points)
        {
            // show err message;
            rewardController.PayoutErrorHandler();
        } else
        {
            // show payout message
            rewardController.PayoutButtonHandler();
        }
    }
    public void ClearImage()
    {
        rewardName.text = string.Empty;
        rewardPrice.text = string.Empty;
    }

    void LoadData(Reward data)
    {
        rewardName.text = data.rewardName;
        price = data.rewardPrice;
        rewardPrice.text = data.rewardPrice + " credits";
        rewardImage.sprite = data.icon;
    }
}
