﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;

[System.Serializable]
public struct ToggleStateData {
    public Sprite image;
    public Color stateColor;
}
public class ToggleButton : MonoBehaviour {


    public ToggleStateData onState;
    public ToggleStateData offState;

    public Image taskLayout;
    public Image mapLayout;

    public UnityEvent onRightEvent;
    public UnityEvent onLeftEvent;

    private bool initState = true;

    private void Start()
    {
        initState = true;
    }

    private void OnEnable()
    {
        initState = true;
        SetState(initState);
    }
    //public methods here:

    public void SetState(bool state)
    {
        if (state)
        {
            // TASK
            taskLayout.sprite = onState.image;
            taskLayout.color = onState.stateColor;

            mapLayout.sprite = offState.image;
            mapLayout.color = offState.stateColor;

            onRightEvent.Invoke();
        } else
        {
            //MAP
            taskLayout.sprite = offState.image;
            taskLayout.color = offState.stateColor;

            mapLayout.sprite = onState.image;
            mapLayout.color = onState.stateColor;

            onLeftEvent.Invoke();
        }
    }

    public void ChangeState()
    {
        if (initState)
        {
            initState = false;
            SetState(initState);
        } else
        {
            initState = true;
            SetState(initState);
        }
    }
}
