﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using EasyMobile;
public class SettingsInfoView : MonoBehaviour {

    [Multiline(10)]
    public string howItWorksText;
    [Multiline(10)]
    public string tosText;
    [Multiline(10)]
    public string helpText;
    [Multiline(10)]
    public string newsFeedText;

    public GameObject problemsDetails;
    public GameObject otherProblemsDetails;
    public GameObject settingsDetail;
    public UnityEngine.UI.Text textReference;

	public void InflateHOW()
    {
        settingsDetail.SetActive(true);
        textReference.text = howItWorksText;
    }

    public void InflateTOS()
    {
        settingsDetail.SetActive(true);
        textReference.text = tosText;
    }

    public void InflateHelp()
    {
        NativeUI.AlertPopup alert = NativeUI.ShowThreeButtonAlert(
            "Help",
            "What sort of help do you need ?",
            "Problem getting credits",
            "Other problems",
            "Cancel"
            );

        if(alert != null)
        {
            alert.OnComplete += OnAlertCompleteHandler;
        }


    }

    public void InflateNews()
    {
        settingsDetail.SetActive(true);
        textReference.text = newsFeedText;
    }

    void OnAlertCompleteHandler(int buttonIndex)
    {
        switch (buttonIndex)
        {
            case 0:
                problemsDetails.SetActive(true);
                break;
            case 1:
                // Other problems
                otherProblemsDetails.SetActive(true);
                break;

            case 2:
                // Back
                break;

            default:

                break;
        }
    }
}
