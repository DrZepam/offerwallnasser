﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using SimpleJSON;

public class OfferObject {

    public string offerid;
    public string name;
    public string name_short;
    public string description;
    public string adcopy;
    public string picture;
    public string payout;
    public string country;
    public string device;
    public string link;
    public string epc;
}
