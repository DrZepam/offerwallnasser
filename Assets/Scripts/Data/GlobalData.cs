﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class GlobalData : MonoBehaviour {

    public static GlobalData Instance;
    public Text topCurrency;
    public Text profileCurrency;

    public int Points { get; private set; }
	// Use this for initialization

    void Awake () {
        Instance = this;
	}

    private void Start()
    {
        InitData();
    }
    // Update is called once per frame
    void Update () {
		
	}

    // private methods

    private void InitData()
    {
        if (PlayerPrefs.HasKey("Points"))
        {
            Points = PlayerPrefs.GetInt("Points");
        } else
        {
            PlayerPrefs.SetInt("Points", 0);
            Points = 0;
        }

        SetPoints(Points);
    }

    public void SetPoints(int value)
    {
        topCurrency.text = value.ToString();
        profileCurrency.text = value.ToString();
    }

    public void AddPoints(int value)
    {
        if (PlayerPrefs.HasKey("Points"))
        {
            Points = PlayerPrefs.GetInt("Points");
            Points += value;
            PlayerPrefs.SetInt("Points", Points);

            topCurrency.text = Points.ToString();
            profileCurrency.text = Points.ToString();
        }
    }
}
