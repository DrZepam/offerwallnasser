﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public struct Reward
{
    public Sprite icon;
    public string rewardName;
    public string rewardPrice;
}

[CreateAssetMenu(fileName = "RewardsData", menuName = "Rewards/RewardsDatabase", order = 1)]
public class RewardDatabase : ScriptableObject {

    public List<Reward> rewardsList;
}
