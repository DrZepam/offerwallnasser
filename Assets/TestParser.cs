﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using SimpleJSON;
public class TestParser : MonoBehaviour {


    public string dataUrl;
    public string cpaUrl;

    public EnhancedScrollerDemos.RemoteResourcesDemo.Controller mainController;
    public EnhancedScrollerDemos.RemoteResourcesDemo.Controller topController;
    private List<OfferObject> offerData;
    private List<OfferObject> cpaData;

	// Use this for initialization
	void Start () {
        offerData = new List<OfferObject>();
        cpaData = new List<OfferObject>();
        AddReferFriend();
        InitData();
	}
	
    private void SetupDevice()
    {
#if UNITY_ANDROID
        dataUrl = dataUrl + "&device=android";
        cpaUrl = cpaUrl + "&device=android";
#endif
#if UNITY_IOS
        dataUrl = dataUrl + "&device=iphone";
        cpaUrl = cpaUrl + "&device=iphone";
#endif
    }
    private void InitData()
    {
        StartCoroutine(GetCountryFromWWW("http://ip-api.com/json"));
        
    }

    IEnumerator GetTextFromWWW(string uri, List<OfferObject> output, EnhancedScrollerDemos.RemoteResourcesDemo.Controller controller, bool limit = false)
    {
        WWW www = new WWW(uri);
        
        yield return www;

        if (www.error != null)
        {
            Debug.Log("Ooops, something went wrong...");
        }
        else
        {
            //textFromWWW = www.text;
            ParseOffers(www.text, output, controller, limit);
        }
    }

    IEnumerator GetCountryFromWWW(string uri)
    {
        WWW www = new WWW(uri);

        yield return www;

        if (www.error != null)
        {
            Debug.Log("Ooops, something went wrong...");
        }
        else
        {
            //textFromWWW = www.text;
            GetCountryCode(www.text);
        }
    }
    
    private void GetCountryCode(string data)
    {
        var json = JSON.Parse(data);
        string countryCode = json["countryCode"].Value;
#if UNITY_ANDROID
        dataUrl = dataUrl + "&country=" + countryCode + "&device=android";
        cpaUrl = cpaUrl + "&country=" + countryCode + "&device=android";
#endif
#if UNITY_IOS
        dataUrl = dataUrl + "&country=" + countryCode + "&device=iphone";
        cpaUrl = cpaUrl + "&country=" + countryCode + "&device=iphone";
#endif

        StartCoroutine(GetTextFromWWW(dataUrl, offerData, mainController));
        StartCoroutine(GetTextFromWWW(cpaUrl, cpaData, topController));

    }
    private void ParseOffers(string jsonData, List<OfferObject> output, EnhancedScrollerDemos.RemoteResourcesDemo.Controller controller, bool limit = false)
    {
        var json = JSON.Parse(jsonData);
        
        JSONArray offers = json["offers"].AsArray;
        int arraySize = 0;
        foreach(JSONNode node in offers)
        {
            OfferObject offer = new OfferObject();
            offer.name_short = node["name_short"];
            offer.name = node["name"];
            offer.picture = node["picture"];
            offer.payout = node["payout"];
            offer.adcopy = node["adcopy"];
            offer.link = node["link"];
            offer.description = node["description"];
            if (!offer.picture.Contains(".gif"))
            {
                if (limit)
                {
                    if (output.Count < 3)
                    {
                        output.Add(offer);
                    }
                } else
                {
                    output.Add(offer);
                }
            }
           
        }
        controller.ReloadController(output);
        //OfferObject[] offerData = JsonHelper.FromJson<OfferObject>(offers);
        //Debug.Log("Loaded " + offerData.Length + " Offers");
    }

    private void AddReferFriend()
    {
        OfferObject refer = new OfferObject();
        refer.name_short = "Invite a friend";
        refer.name = "Invite a friend";
        refer.payout = "1";
        refer.adcopy = "Invite a friend and earn 100 points";
        refer.link = "";

        offerData.Add(refer);
        
    }
}
