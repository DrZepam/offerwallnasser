﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using EasyMobile;

public class TaskDetailView : MonoBehaviour {

    [Header("UI References")]
    public Image offerImage;
    public Text offerName;
    public Text offerPayoutText;
    public Text offerDescription;
    public Button taskButton;

    public Sprite referSprite;
    private float payout;
    private string url;
    [Multiline(5)]
    public string shareMessage;

    public void InflateView(OfferObject data)
    {
        StartCoroutine(LoadRemoteImage(data.picture));
        offerName.text = data.name_short;
        payout = (float.Parse(data.payout) * 100);
        offerPayoutText.text = "Earn " + (float.Parse(data.payout) * 100).ToString() + " cr";
        //offerDescription.text = data.name;
        url = data.link;
        taskButton.onClick.AddListener(OpenLink);
    }

    public void ReferInflateView(OfferObject data)
    {
        payout = (float.Parse(data.payout) * 100);
        offerName.text = data.name_short;
        offerPayoutText.text = "Earn " + (float.Parse(data.payout) * 100).ToString() + " cr";
        taskButton.onClick.AddListener(ShareLink);
        offerImage.sprite = referSprite;

        GetComponent<CanvasGroup>().alpha = 1;
    }

    IEnumerator LoadRemoteImage(string url)
    {
        TextureCache cache = new TextureCache();
        string path = url;
        if (!cache.HasCache(path))
        {
            WWW www = new WWW(path);
            yield return www;
            cache.WriteCache(www);

            offerImage.sprite = Sprite.Create(www.texture, new Rect(0, 0, www.texture.width, www.texture.height), new Vector2(0, 0));
        }
        else
        {
            WWW www = new WWW(cache.CacheUrl(path));
            yield return www;

            offerImage.sprite = Sprite.Create(www.texture, new Rect(0, 0, www.texture.width, www.texture.height), new Vector2(0, 0));
        }

        GetComponent<CanvasGroup>().alpha = 1;
    }

    void OpenLink()
    {
        GlobalData.Instance.AddPoints(Mathf.RoundToInt(payout));
        Debug.Log("Added " + payout + " Points");
        Application.OpenURL(url);
    }

    void ShareLink()
    {
        GlobalData.Instance.AddPoints(Mathf.RoundToInt(payout));
        Debug.Log("Added " + payout + " Points");
        Sharing.ShareText(shareMessage);
    }
}
