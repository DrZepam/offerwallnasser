# Offerwall Application
This is a simple offerwall application made inside unity.
It uses Unity version 2017.3 for building.
Also depends on Enhanced Scroller for better scrollviews, EasyMobile for native AlertDialog and Sharing.


## Configuration
in the main scene, OGAds api links for both CPI and CPA offers are included in the Parser object
Screen switching uses standard UnityEvents and events are dragged from the inspector.

