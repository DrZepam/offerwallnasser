﻿using UnityEngine;
using UnityEngine.UI;
using EnhancedUI.EnhancedScroller;
using System.Collections;
using EasyMobile;

namespace EnhancedScrollerDemos.RemoteResourcesDemo
{
    public class CellView : EnhancedScrollerCellView
    {
        public Image cellImage;
        public Text name_short;
        public Text adcopy;
        public Text payoutText;
        public Button offerButton;
        public Sprite defaultSprite;
        public Sprite referSprite;
        

        private string link;
        private OfferObject offerData;
        private Controller taskController;
        public void SetData(OfferObject data, Controller controller)
        {
            taskController = controller;
            StartCoroutine(LoadRemoteImage(data));
        }

        public IEnumerator LoadRemoteImage(OfferObject data)
        {
            if (!data.name.Contains("Invite"))
            {
                TextureCache cache = new TextureCache();
                offerData = data;
                name_short.text = data.name_short;
                if (adcopy != null) { adcopy.text = data.adcopy; };
                payoutText.text = "+" + (float.Parse(data.payout) * 100).ToString();
                link = data.link;
                Debug.Log("Offer Link: " + link);
                offerButton.onClick.AddListener(OpenLink);
                string path = data.picture;

                if (!cache.HasCache(path))
                {
                    WWW www = new WWW(path);
                    yield return www;
                    cache.WriteCache(www);

                    cellImage.sprite = Sprite.Create(www.texture, new Rect(0, 0, www.texture.width, www.texture.height), new Vector2(0, 0));
                }
                else
                {
                    WWW www = new WWW(cache.CacheUrl(path));
                    yield return www;

                    cellImage.sprite = Sprite.Create(www.texture, new Rect(0, 0, www.texture.width, www.texture.height), new Vector2(0, 0));
                }
            } else
            {
                offerData = data;
                name_short.text = data.name_short;
                if (adcopy != null) { adcopy.text = data.adcopy; };
                payoutText.text = "+" + (float.Parse(data.payout) * 100).ToString();
                offerButton.onClick.AddListener(ShareLink);
                cellImage.sprite = referSprite;
            }
        }

        public void ClearImage()
        {
            name_short.text = string.Empty;
            if (adcopy != null) { adcopy.text = string.Empty; }
            payoutText.text = string.Empty;
            link = string.Empty;
            offerButton.onClick.RemoveAllListeners();
            cellImage.sprite = defaultSprite;
        }

        void OpenLink()
        {
            taskController.ShowTaskDetails(offerData);
        }

        void ShareLink()
        {
            taskController.ReferTaskDetails(offerData);
        }
    }
}