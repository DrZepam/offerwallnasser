﻿using UnityEngine;
using System;
using System.Collections;
using System.IO;

/// <summary>
///	
/// </summary>
public class TextureCache
{
    string strToBase64(string str)
    {
        byte[] byt = System.Text.Encoding.UTF8.GetBytes(str);
        return Convert.ToBase64String(byt);
    }

    string base64ToStr(string base64)
    {
        byte[] b = Convert.FromBase64String(base64);
        return System.Text.Encoding.UTF8.GetString(b);
    }

    string urlToCachePath(string url)
    {
        return Application.persistentDataPath + "/" + strToBase64(url);
    }

    public bool HasCache(string url)
    {
        return File.Exists(urlToCachePath(url));
    }

    public void WriteCache(WWW www)
    {
        File.WriteAllBytes(urlToCachePath(www.url), www.bytes);
    }

    public string CacheUrl(string url)
    {
        string path = urlToCachePath(url);
        if (File.Exists(path))
        {
            return "file://" + path;
        }
        Debug.LogWarning("dont have cache url:" + url);
        return "";
    }
}
